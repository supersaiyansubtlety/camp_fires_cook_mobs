- 1.4.1 (17 Dec. 2024):
  - Marked as compatible with 1.21.4
  - Fixed a crash that occurred when [On Soul Fire](<https://modrinth.com/mod/on-soul-fire>) was present but
  [Cloth Config](<https://modrinth.com/mod/cloth-config>) was not
  - Moderate internal changes
- 1.4.0 (1 Dec. 2024):
  - Updated for 1.21.2-1.21.3
  - Restored `integrate_with_on_soul_fire` config
- 1.3.1 (19 Aug. 2024):
  - Marked as compatible with 1.21.1
  - Re-enable [On Soul Fire](<https://modrinth.com/mod/on-soul-fire>) integration
- 1.3.0 (17 Jul. 2024):
  - Updated for 1.21!
  - Disabled [On Soul Fire](<https://modrinth.com/mod/on-soul-fire>) integration as it hasn't been updated yet
  - Replaced bundled [CrowdinTranslate](<https://github.com/gbl/CrowdinTranslate>) with optional
  [SSS Translate](<https://modrinth.com/mod/sss-translate>) dependency;
  install [SSS Translate](<https://modrinth.com/mod/sss-translate>) for automatic translation updates
  - Moderate internal changes (campfires actually use the "campfire" damage type now)
- 1.2.15 (3 Jun. 2024): Restored [On Soul Fire](https://modrinth.com/mod/on-soul-fire) integration.
- 1.2.14 (21 May 2024):
  - Updated for 1.20.5 and 1.20.6
  - Disabled [On Soul Fire](https://modrinth.com/mod/on-soul-fire) integration as it hasn't been updated yet
  - Made cloth config *actually* optional and added text to the 'no config' screen (oops)
  - Improved [Mod Menu](https://modrinth.com/mod/modmenu) integration
- 1.2.13 (2 Feb. 2024):
  - Marked as compatible with 1.20.3 and 1.20.4
  - Minor internal changes
- 1.2.12 (13 Nov. 2023): Updated for 1.20.2
- 1.2.11 (19 Jun. 2023): Updated for 1.20 and 1.20.1!
- 1.2.10 (23 Mar. 2023): Updated for 1.19.4
- 1.2.9 (7 Aug. 2022): Marked as compatible with 1.19.2
- 1.2.8 (29 Jul. 2022): Marked as compatible with 1.19.1
- 1.2.7 (28 Jun. 2022): Updated for 1.19!
- 1.2.7-a1 (20 Jun. 2022): Add option to disable On Soul Fire integration.
- 1.2.6 (29 May 2022): Fixes inconsistent player soulfire rendering and broken mob soulfire rendering with onsoulfire installed. 
Thanks to [RoseTheFoxGit](https://gitlab.com/RoseTheFoxGit) for reporting this issue.

- 1.2.5 (28 May 2022): Now compatible with On Soul Fire 1.18-3. Previous versions won't provide a soul fire overlay on soul campfires.

- 1.2.4 (23 May 2022):
  
  Updated for 1.18.2!

  Cloth Config is now optional.
  
- 1.2.3 (13 Dec. 2021): Marked as compatible with 1.18.1
- 1.2.2 (3 Dec. 2021): Updated for 1.18!
- 1.2.1 (9 Aug. 2021): 
  - Fixed compatibility with the latest version of On Soul Fire. Version 1.17-3 or newer of On Soul Fire is now required for the blue fire rendering, but using an older version will no longer cause a crash. 
  - If you have On Soul Fire 1.17-3+, entities will render blue flames after they step off of soul campfires (until the fire goes out, they step on a non-soul flame, or death)
- 1.2 (1 Aug. 2021): 
  - all configs can now be changed freely without restarting the game!
  - removed autoconfig dependency (it's been incorporated into recent cloth config releases)
  - cleaned up some code
- 1.1.10 (13 Jul. 2021): 
  Entities standing on soul camp fires will now be covered in blue flames if you have [On Soul Fire](https://www.curseforge.com/minecraft/mc-mods/on-soul-fire) installed. 
  Thanks to [SnapieBee](https://www.curseforge.com/members/snapiebee) for suggesting this integration!
- 1.1.9 (9 Jul. 2021): Marked as compatible with 1.17.1.
- 1.1.8 (16 Jun. 2021): 
  - Updated for 1.17. 
  - Cloth Config and Auto Config (updated) are no longer bundled. Instead, install them separately. 
- 1.1.7 (07 Feb. 2021): Crowdin can be disabled through the config file (thanks to [Fourmisain](https://gitlab.com/Fourmisain)!)
- 1.1.6 (15 Jan. 2021): Translations are now handled through [CrowdinTranslate](https://crowdin.com/project/camp-fire-cook-mobs).
  Marked as compatible with 1.16.5. 
- 1.1.5 (3 Nov. 2020): Marked as compatible with 1.16.4. 
- 1.1.4 (18 Sep. 2020): Marked as compatible with 1.16.3. 