package net.sssubtlety.camp_fires_cook_mobs.mixin_helper;

import net.minecraft.entity.damage.DamageSource;
import net.minecraft.world.World;

import java.util.function.BiPredicate;

public interface DamageSourcePredicateMixinAccessor {
    void camp_fires_cook_mobs$setAdditionalPredicate(BiPredicate<World, DamageSource> predicate);
}
