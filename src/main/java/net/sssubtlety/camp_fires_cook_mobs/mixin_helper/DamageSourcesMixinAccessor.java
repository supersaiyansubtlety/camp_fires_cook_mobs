package net.sssubtlety.camp_fires_cook_mobs.mixin_helper;

import net.minecraft.entity.damage.DamageSource;

public interface DamageSourcesMixinAccessor {
    DamageSource camp_fires_cook_mobs$getCampfireDamagePredicateIgnored();
}
