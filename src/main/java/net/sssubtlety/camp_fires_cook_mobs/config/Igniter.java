package net.sssubtlety.camp_fires_cook_mobs.config;

import net.minecraft.entity.Entity;

public sealed class Igniter permits OnSoulFireIgniter {
    Igniter() { }

    public void ignite(Entity entity, boolean soul) {
        entity.setFireTicks(entity.getFireTicks() + 1);

        if (entity.getFireTicks() == 0) {
            entity.setOnFireFor(8);
        }
    }
}
