package net.sssubtlety.camp_fires_cook_mobs.config;

import moriyashiine.onsoulfire.common.component.entity.OnSoulFireComponent;
import moriyashiine.onsoulfire.common.init.ModEntityComponents;
import net.minecraft.entity.Entity;

import static net.sssubtlety.camp_fires_cook_mobs.config.ConfigManager.shouldIntegrateWithOnSoulFire;

final class OnSoulFireIgniter extends Igniter {
    OnSoulFireIgniter() { }

    @Override
    public void ignite(Entity entity, boolean soul) {
        super.ignite(entity, soul);

        if (!shouldIntegrateWithOnSoulFire()) {
            return;
        }

        final OnSoulFireComponent onSoulFireComponent = ModEntityComponents.ON_SOUL_FIRE.get(entity);
        if (onSoulFireComponent.isOnSoulFire() != soul) {
            onSoulFireComponent.setOnSoulFire(soul);
            onSoulFireComponent.sync();
        }
    }
}
