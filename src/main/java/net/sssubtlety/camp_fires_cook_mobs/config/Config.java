package net.sssubtlety.camp_fires_cook_mobs.config;

import net.minecraft.client.gui.screen.Screen;

import java.util.function.Function;

public interface Config {
    boolean standardCampfiresBurnItems();

    boolean soulCampfiresBurnItems();

    boolean frostWalkerProtectsFromStandardCampfires();

    boolean frostWalkerProtectsFromSoulCampfires();

    boolean integrateWithOnSoulFire();

    Function<Screen, ? extends Screen> screenFactory();
}
