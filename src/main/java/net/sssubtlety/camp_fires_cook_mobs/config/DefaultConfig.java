package net.sssubtlety.camp_fires_cook_mobs.config;

import net.sssubtlety.camp_fires_cook_mobs.CampFiresCookMobs;
import net.sssubtlety.camp_fires_cook_mobs.Util;

import net.minecraft.client.gui.GuiGraphics;
import net.minecraft.client.gui.screen.Screen;
import net.minecraft.text.Text;
import net.minecraft.util.Formatting;

import java.util.function.Function;

import static net.sssubtlety.camp_fires_cook_mobs.CampFiresCookMobs.NAMESPACE;

public final class DefaultConfig implements Config {
    public static final DefaultConfig INSTANCE = new DefaultConfig();

    private DefaultConfig() { }

    @Override
    public boolean standardCampfiresBurnItems() {
        return true;
    }

    @Override
    public boolean soulCampfiresBurnItems() {
        return false;
    }

    @Override
    public boolean frostWalkerProtectsFromStandardCampfires() {
        return true;
    }

    @Override
    public boolean frostWalkerProtectsFromSoulCampfires() {
        return false;
    }

    @Override
    public boolean integrateWithOnSoulFire() {
        return true;
    }

    @Override
    public Function<Screen, ? extends Screen> screenFactory() {
        return NoConfigScreen::new;
    }

    public static final class NoConfigScreen extends Screen {
        // WHITE's color value is not null
        @SuppressWarnings("DataFlowIssue")
        private static final int TITLE_COLOR = Formatting.WHITE.getColorValue();
        // RED's color value is not null
        @SuppressWarnings("DataFlowIssue")
        private static final int MESSAGE_COLOR = Formatting.RED.getColorValue();
        private static final Text NO_CONFIG_SCREEN_TITLE =
            Text.translatable("text." + NAMESPACE + ".no_config_screen.title");
        private static final Text NO_CONFIG_SCREEN_MESSAGE =
            Text.translatable("text." + NAMESPACE + ".no_config_screen.message");

        private final Screen parent;
        public NoConfigScreen(Screen parent) {
            super(NO_CONFIG_SCREEN_TITLE);
            this.parent = parent;
        }

        @Override
        public void render(GuiGraphics graphics, int mouseX, int mouseY, float delta) {
            super.render(graphics, mouseX, mouseY, delta);
            final int horizontalCenter = this.width / 2;

            graphics.drawCenteredShadowedText(
                this.textRenderer,
                Util.replace(NO_CONFIG_SCREEN_TITLE, "\\$\\{name\\}", CampFiresCookMobs.NAME.getString()),
                horizontalCenter, this.height / 10,
                TITLE_COLOR
            );

            graphics.drawCenteredShadowedText(
                this.textRenderer,
                NO_CONFIG_SCREEN_MESSAGE,
                horizontalCenter, this.height / 2,
                MESSAGE_COLOR
            );
        }

        @SuppressWarnings("ConstantConditions")
        @Override
        public void closeScreen() {
            this.client.setScreen(this.parent);
        }
    }
}
