package net.sssubtlety.camp_fires_cook_mobs.config;

import net.sssubtlety.camp_fires_cook_mobs.CampFiresCookMobs;

import me.shedaniel.autoconfig.AutoConfig;
import me.shedaniel.autoconfig.ConfigData;
import me.shedaniel.autoconfig.serializer.GsonConfigSerializer;

import net.minecraft.client.gui.screen.Screen;

import java.util.function.Function;

@me.shedaniel.autoconfig.annotation.Config(name = CampFiresCookMobs.NAMESPACE)
class ClothConfig implements ConfigData, Config {
    static ClothConfig register() {
        return AutoConfig.register(ClothConfig.class, GsonConfigSerializer::new).getConfig();
    }

    boolean standard_campfires_burn_items = DefaultConfig.INSTANCE.standardCampfiresBurnItems();

    boolean soul_campfires_burn_items = DefaultConfig.INSTANCE.soulCampfiresBurnItems();

    boolean frost_walker_protects_from_standard_campfires =
        DefaultConfig.INSTANCE.frostWalkerProtectsFromStandardCampfires();

    boolean frost_walker_protects_from_soul_campfires =
        DefaultConfig.INSTANCE.frostWalkerProtectsFromSoulCampfires();

    boolean integrate_with_on_soul_fire = DefaultConfig.INSTANCE.integrateWithOnSoulFire();

    @Override
    public boolean standardCampfiresBurnItems() {
        return this.standard_campfires_burn_items;
    }

    @Override
    public boolean soulCampfiresBurnItems() {
        return this.soul_campfires_burn_items;
    }

    @Override
    public boolean frostWalkerProtectsFromStandardCampfires() {
        return this.frost_walker_protects_from_standard_campfires;
    }

    @Override
    public boolean frostWalkerProtectsFromSoulCampfires() {
        return this.frost_walker_protects_from_soul_campfires;
    }

    @Override
    public boolean integrateWithOnSoulFire() {
        return this.integrate_with_on_soul_fire;
    }

    @Override
    public Function<Screen, ? extends Screen> screenFactory() {
        return parent -> AutoConfig.getConfigScreen(ClothConfig.class, parent).get();
    }
}
