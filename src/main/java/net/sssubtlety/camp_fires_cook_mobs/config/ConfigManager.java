package net.sssubtlety.camp_fires_cook_mobs.config;

import net.minecraft.client.gui.screen.Screen;

import java.util.function.Function;

import static net.sssubtlety.camp_fires_cook_mobs.Util.isModLoaded;

public final class ConfigManager {
    private ConfigManager() { }

    private static final boolean COMPATIBLE_ON_SOUL_FIRE_PRESENT =
        isModLoaded("onsoulfire", ">=1.21.2-r1");

    private static final Config CONFIG = isModLoaded("cloth-config", ">=6.1.48") ?
        ClothConfig.register() : DefaultConfig.INSTANCE;

    public static final Igniter IGNITER = COMPATIBLE_ON_SOUL_FIRE_PRESENT ?
        new OnSoulFireIgniter() : new Igniter();

    public static void bootstrap() { }

    public static boolean doStandardCampFiresBurnItems() {
        return CONFIG.standardCampfiresBurnItems();
    }

    public static boolean doSoulCampFiresBurnItems() {
        return CONFIG.soulCampfiresBurnItems();
    }

    public static boolean doesFrostWalkerProtectFromStandardCampFires() {
        return CONFIG.frostWalkerProtectsFromStandardCampfires();
    }

    public static boolean doesFrostWalkerProtectFromSoulCampFires() {
        return CONFIG.frostWalkerProtectsFromSoulCampfires();
    }

    public static boolean shouldIntegrateWithOnSoulFire() {
        return COMPATIBLE_ON_SOUL_FIRE_PRESENT && CONFIG.integrateWithOnSoulFire();
    }

    public static Function<Screen, ? extends Screen> getScreenFactory() {
        return CONFIG.screenFactory();
    }
}
