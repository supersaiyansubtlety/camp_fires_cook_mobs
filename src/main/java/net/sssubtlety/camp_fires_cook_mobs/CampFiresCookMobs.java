package net.sssubtlety.camp_fires_cook_mobs;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.minecraft.text.Text;

public class CampFiresCookMobs {
    public static final String NAMESPACE = "camp_fires_cook_mobs";
    public static final Text NAME = Text.translatable("text." + NAMESPACE + ".name");
    public static final Logger LOGGER = LoggerFactory.getLogger(NAMESPACE);
}
