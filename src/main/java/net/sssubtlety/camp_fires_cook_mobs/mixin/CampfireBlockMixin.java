package net.sssubtlety.camp_fires_cook_mobs.mixin;

import com.llamalad7.mixinextras.injector.ModifyExpressionValue;
import com.llamalad7.mixinextras.injector.v2.WrapWithCondition;
import com.llamalad7.mixinextras.sugar.Share;
import com.llamalad7.mixinextras.sugar.ref.LocalBooleanRef;
import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.block.CampfireBlock;
import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.entity.Entity;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.damage.DamageSource;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

import net.sssubtlety.camp_fires_cook_mobs.config.ConfigManager;
import net.sssubtlety.camp_fires_cook_mobs.mixin_helper.DamageSourcesMixinAccessor;
import org.spongepowered.asm.mixin.Final;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;

import static net.sssubtlety.camp_fires_cook_mobs.config.ConfigManager.doSoulCampFiresBurnItems;
import static net.sssubtlety.camp_fires_cook_mobs.config.ConfigManager.doStandardCampFiresBurnItems;
import static net.sssubtlety.camp_fires_cook_mobs.config.ConfigManager.doesFrostWalkerProtectFromSoulCampFires;
import static net.sssubtlety.camp_fires_cook_mobs.config.ConfigManager.doesFrostWalkerProtectFromStandardCampFires;

import static java.lang.Boolean.TRUE;
import static net.minecraft.enchantment.Enchantments.FROST_WALKER;
import static net.minecraft.registry.RegistryKeys.ENCHANTMENT;

@Mixin(value = CampfireBlock.class, priority = 500)
abstract class CampfireBlockMixin {
	@Shadow
	@Final
	private int fireDamage;

	// for entity::damage
	@SuppressWarnings("deprecation")
    @ModifyExpressionValue(
		method = "onEntityCollision",
		at = @At(
			value = "INVOKE",
			target = "Lnet/minecraft/block/BlockState;getOrThrow(Lnet/minecraft/state/property/Property;)" +
				"Ljava/lang/Comparable;"
		)
	)
	private Comparable<Boolean> burnNonLivingEntityIfEnabled(
		Comparable<Boolean> stateLit, BlockState state, World world, BlockPos pos, Entity entity,
		@Share("soulCampfire") LocalBooleanRef soulCampfire
	) {
		if (TRUE.equals(stateLit)) {
			soulCampfire.set(state.getBlock() == Blocks.SOUL_CAMPFIRE);

			if (
				(!(entity instanceof LivingEntity)) && (
					soulCampfire.get() ?
						doSoulCampFiresBurnItems() :
						doStandardCampFiresBurnItems()
				)
			) {
				ConfigManager.IGNITER.ignite(entity, soulCampfire.get());
                entity.damage(world.getDamageSources().campfire(), this.fireDamage);
			}
		}

		return stateLit;
	}

	@WrapWithCondition(
		method = "onEntityCollision",
		at = @At(
			value = "INVOKE",
			target = "Lnet/minecraft/entity/Entity;damage(Lnet/minecraft/entity/damage/DamageSource;F)V"
		)
	)
	private boolean setOnFireOrPreventDamageIfFrostWalkerProtects(
		Entity entity, DamageSource source, float amount,
		@Share("soulCampfire") LocalBooleanRef soulCampfire,
		@Share("frostWalkerCanProtect") LocalBooleanRef frostWalkerCanProtect
	) {
		frostWalkerCanProtect.set(
			soulCampfire.get() ?
				doesFrostWalkerProtectFromSoulCampFires() :
				doesFrostWalkerProtectFromStandardCampFires()
		);

		final boolean frostWalkerProtects = frostWalkerCanProtect.get() && (
			// entity has frost walker
			entity instanceof LivingEntity livingEntity &&
				entity.getWorld().getRegistryManager().getLookup(ENCHANTMENT)
					.flatMap(enchantments -> enchantments.getHolder(FROST_WALKER))
					.filter(frostWalker ->
						EnchantmentHelper.getHighestEquippedLevel(frostWalker, livingEntity) > 0
					).isPresent()
		);

		if (frostWalkerProtects) {
			return false;
		} else {
			ConfigManager.IGNITER.ignite(entity, soulCampfire.get());
			return true;
		}
	}

	@ModifyExpressionValue(
		method = "onEntityCollision",
		at = @At(
			value = "INVOKE",
			target = "Lnet/minecraft/entity/damage/DamageSources;campfire()Lnet/minecraft/entity/damage/DamageSource;"
		)
	)
	private DamageSource tryIgnoreFrostwalker(
		DamageSource original,
		BlockState state, World world,
		@Share("frostWalkerCanProtect") LocalBooleanRef frostWalkerCanProtect
	) {
		return frostWalkerCanProtect.get() ?
			original :
			((DamageSourcesMixinAccessor)world.getDamageSources())
				.camp_fires_cook_mobs$getCampfireDamagePredicateIgnored();
	}
}
