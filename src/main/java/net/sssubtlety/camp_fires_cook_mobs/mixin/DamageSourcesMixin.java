package net.sssubtlety.camp_fires_cook_mobs.mixin;

import net.minecraft.entity.damage.DamageSource;
import net.minecraft.entity.damage.DamageSources;
import net.minecraft.entity.damage.DamageType;
import net.minecraft.entity.damage.DamageTypes;
import net.minecraft.registry.RegistryKey;
import net.sssubtlety.camp_fires_cook_mobs.mixin_helper.DamageSourcesMixinAccessor;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.Unique;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

@Mixin(DamageSources.class)
abstract class DamageSourcesMixin implements DamageSourcesMixinAccessor {
    @Shadow public abstract DamageSource create(RegistryKey<DamageType> registryKey);

    @Unique private DamageSource campfireDamagePredicateIgnored;

    @Override
    public DamageSource camp_fires_cook_mobs$getCampfireDamagePredicateIgnored() {
        return this.campfireDamagePredicateIgnored;
    }

    @Inject(method = "<init>", at = @At("TAIL"))
    private void initFields(CallbackInfo ci) {
        this.campfireDamagePredicateIgnored = this.create(DamageTypes.CAMPFIRE);
    }
}
