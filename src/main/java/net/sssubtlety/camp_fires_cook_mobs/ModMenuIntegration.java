package net.sssubtlety.camp_fires_cook_mobs;

import net.sssubtlety.camp_fires_cook_mobs.config.ConfigManager;

import com.terraformersmc.modmenu.api.ConfigScreenFactory;
import com.terraformersmc.modmenu.api.ModMenuApi;

import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;

@Environment(EnvType.CLIENT)
public class ModMenuIntegration implements ModMenuApi {
    @Override
    public ConfigScreenFactory<?> getModConfigScreenFactory() {
        return ConfigManager.getScreenFactory()::apply;
    }
}
