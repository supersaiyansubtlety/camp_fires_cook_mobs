package net.sssubtlety.camp_fires_cook_mobs;

import net.sssubtlety.camp_fires_cook_mobs.config.ConfigManager;

import net.fabricmc.api.ModInitializer;

public class Init implements ModInitializer {
    @Override
    public void onInitialize() {
        ConfigManager.bootstrap();
    }
}
